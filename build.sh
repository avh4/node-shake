#!/bin/bash

set -euxo pipefail

docker build . -t avh4/node-shake
docker run avh4/node-shake node --version
docker run avh4/node-shake ghc --version
docker run avh4/node-shake cabal --version
docker run avh4/node-shake shake --version
docker run avh4/node-shake elm-tooling | tail -n2

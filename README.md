[Node.js](https://hub.docker.com/_/node) plus [Haskell](https://hub.docker.com/_/haskell) and [shake](https://shakebuild.com).
The primary use case is for [Elm](https://elm-lang.org) projects that want to use shake as the build system.

This is currently based on [debian:buster](https://hub.docker.com/_/debian) (via the node:buster image),
though using that base is not a core goal of this image.

Pull requests are welcome for updated node/Haskell/shake versions
or for additions of Haskell libraries that generally are useful in Shakefiles: https://gitlab.com/avh4/node-shake

Included tools:

- Node.js: node, npm (everything from the official [node docker image](https://hub.docker.com/_/node))
- Haskell: ghc, cabal, stack (everything from the official [haskell docker image](https://hub.docker.com/_/haskell))
- [shake](https://shakebuild.com) "Build system library, like Make, but more accurate dependencies"
- [pnpm](https://pnpm.io) "Fast, disk space efficient [node] package manager"
- [elm-tooling](https://elm-tooling.github.io/elm-tooling-cli/) "manages your Elm tools"
